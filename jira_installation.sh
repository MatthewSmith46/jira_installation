#!/bin/bash

#get some parameters for database
echo "JIRA database name:"
read DB_NAME
echo "JIRA database username:"
read DB_USER
echo "JIRA database password:"
read DB_PASS

# settings for jira and jdbc drivers
jiraversion="8.19.0"
jirabuild="atlassian-jira-software-$jiraversion"
jiradownload="$jirabuild.tar.gz"
jdbcversion="postgresql-42.2.5"
jdbcdownload="http://jdbc.postgresql.org/download/$jdbcversion.jar"

#install some utilities
sudo apt-get install unzip ed -y
sudo apt install default-jdk -y

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

#Create Jira user
sudo adduser --system --shell /bin/bash --gecos 'JIRA owner' --group --disabled-password --home /srv/jira jira

#Installing postgresql
sudo apt -y install postgresql-12 postgresql-client-12

# create postgresql user for jira
sudo su postgres <<EOF
createdb $DB_NAME;
psql -c "CREATE USER $DB_USER with PASSWORD '$DB_PASS';"
psql -c "GRANT ALL PRIVILEGES on DATABASE $DB_NAME to $DB_USER;"
EOF

# download postgresql jdbc driver
sudo wget -O /usr/share/java/$jdbcversion.jar $jdbcdownload
sudo ln -sf /usr/share/java/$jdbcversion.jar

# download jira
sudo su - -c "wget https://www.atlassian.com/software/jira/downloads/binary/$jiradownload" jira
sudo su - -c "tar -xvzf $jiradownload" jira
sudo su - -c "rm $jiradownload" jira
sudo su - -c "mv $jirabuild-standalone build" jira

sudo su - -c "mkdir jira-home" jira
sudo sed -i 's/jira.home =/jira.home = \/srv\/jira\/jira-home/' /srv/jira/build/atlassian-jira/WEB-INF/classes/jira-application.properties

sudo sed -i 's/field-type-name="hsql"/field-type-name="postgres72"/' /srv/jira/build/atlassian-jira/WEB-INF/classes/entityengine.xml
sudo sed -i 's/schema-name="PUBLIC"/schema-name="public"/' /srv/jira/build/atlassian-jira/WEB-INF/classes/entityengine.xml

# create dbconfig.xml
sudo su - -c "touch /srv/jira/jira-home/dbconfig.xml" jira
sudo ed /srv/jira/jira-home/dbconfig.xml << EOF
i
<?xml version="1.0" encoding="UTF-8"?>

<jira-database-config>
  <name>defaultDS</name>
  <delegator-name>default</delegator-name>
  <database-type>postgres72</database-type>
  <schema-name>public</schema-name>
  <jdbc-datasource>
    <url>jdbc:postgresql://localhost:5432/$DB_NAME/url>
    <driver-class>org.postgresql.Driver</driver-class>
    <username>$DB_USER</username>
    <password>$DB_PASS</password>
    <pool-min-size>20</pool-min-size>
    <pool-max-size>20</pool-max-size>
    <pool-max-wait>30000</pool-max-wait>
    <validation-query>select 1</validation-query>
    <min-evictable-idle-time-millis>60000</min-evictable-idle-time-millis>
    <time-between-eviction-runs-millis>300000</time-between-eviction-runs-millis>
    <pool-max-idle>20</pool-max-idle>
    <pool-remove-abandoned>true</pool-remove-abandoned>
    <pool-remove-abandoned-timeout>300</pool-remove-abandoned-timeout>
    <pool-test-on-borrow>false</pool-test-on-borrow>
    <pool-test-while-idle>true</pool-test-while-idle>
  </jdbc-datasource>
</jira-database-config>
.
w
q
EOF

# build jira instance
sudo su - -c 'cd /srv/jira/build/bin;sh start-jira.sh' jira