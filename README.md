## Engineering Assignment ISOS

For this assignment I have created a bash script that goes through the installation steps to setup a Jira instance on Ubuntu 20.4.

# The script jira_installation.sh

1. Collect information to create the database
2. Downloads the correct JDBC drivers
3. Install the latest java JDK and sets the JAVE_HOME environment variable.
4. Create a Jira user and set the home directory to /srv/jira
5. Install and configure the PostgreSQL database with the information that was collected.
6. Download the Jira archive file and extract the content of the file.
7. Make the Jira home directory and set the directory in the jira-application.properties file.
8. Create dbconfig.xml file and add database configuration.
9. Stat Jira by running the start-jira.sh

---

# Issues I've encounter

Some of the issue I've encounter while trying to install

1. When I first tried to run the start-jira.sh it would fail because it didn't set the JAVA_HOME.
	- Figured out what environment variable needed to be set and added to script.

2. Even though I have created the dbconfig.xml file via my shell script was unable to get Jira to load the database config.
	- Looking through the documentation was still unable to get the database to load automatically. Added this to the improvement steps.

---

# Improvements I would like to see

1. Automate the entire configuration of Jira so that when you go to the site for the first time everything is configured.
	- I'm not sure if this can be accomplished with the archive file. I've look at the unattended file.

2. While I've created the dbcofig.xml file via the script. When I start Jira it doesn't seem to recognize the file and connect into the database.
	- Looking through the documentation the only way to connect the database at launch is with the wizard or running the configuration tool.
	
3. Be able to load the sample data provided automatically.

---

# Screen Shots

## Screen Shot of project in Jira instance
![Scheme](https://bitbucket.org/MatthewSmith46/jira_installation/raw/master/jira_ui.png)

## Screen Shots of System Info
![Scheme](https://bitbucket.org/MatthewSmith46/jira_installation/raw/master/jira_sys_info1.png)
![Scheme](https://bitbucket.org/MatthewSmith46/jira_installation/raw/master/jira_sys_info2.png)